/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structure;

/**
 *
 * @author Ahmad
 */
public class News {
    
    public int id;
    
    public String title;
    public String url;
    public String pubDate;
    public String content;
    public String imageLink;
    public String category;
    
    // 5W1H
    public String what;
    public String who;
    public String where;
    public String when;
    public String why;
    public String how;
    
    public News() {
        
    }

    public String getContent() {
        return content;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhat() {
        return what;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getWhen() {
        return when;
    }

    public void setHow(String how) {
        this.how = how;
    }

    public String getHow() {
        return how;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getWhere() {
        return where;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getWho() {
        return who;
    }

    public void setWhy(String why) {
        this.why = why;
    }

    public String getWhy() {
        return why;
    }
    
}
