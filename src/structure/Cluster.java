/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package structure;

import database.DatabaseHelper;

/**
 * Representasi entity set cluster.
 * @author Bagus Rahman Aryabima
 */
public class Cluster {
    /**
     * Memasukkan entity cluster baru.
     * @param id_cluster Atribut id_cluster entity cluster baru.
     * @param id_kelas Atribut id_kelas entity cluster baru.
     */
    
    /*
      
-- Dumping structure for table newsaggregator.cluster
CREATE TABLE IF NOT EXISTS `cluster` (
  `ID_CLUSTER` int(11) NOT NULL AUTO_INCREMENT,
  `ID_KELAS` int(11) DEFAULT NULL,
  `RINGKASAN_STANDARD` text,
  `CLUSTER_NAME` text NOT NULL,
  `ID_MEDOID` int(11) DEFAULT NULL COMMENT 'id artikel sbg medoid cluster',
  `MIN_DIST` double DEFAULT NULL COMMENT 'punya medoid',
  `MAX_DIST` double DEFAULT NULL,
  `AVG_DIST` double DEFAULT NULL,
  `isFeasible` tinyint(1) DEFAULT NULL COMMENT '1 artinya feasible untuk ditampilkan',
  `centroid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID_CLUSTER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



    
    */
    
    
    public static void insertCluster(String id_cluster, String id_kelas, String id_artikel, double minDist, double maxDist, double avgDist, boolean isFeasible, String centroid) {
        String query = "INSERT IGNORE INTO cluster (ID_CLUSTER,ID_KELAS,RINGKASAN_STANDARD,"
                + "CLUSTER_NAME,ID_MEDOID,MIN_DIST,MAX_DIST,AVG_DIST,isFeasible,centroid) VALUES ('"
                + id_cluster + "', '"
                + id_kelas + "', "
                + "NULL, "
                + "NULL,"
                +id_artikel+","
                +minDist+","
                +maxDist+","
                +avgDist+","
                +isFeasible+",'"
                +centroid+"')";
                
                //yw: kelebihan
                //+ ",NULL)";
        DatabaseHelper.execute(query);
    }
    
    /**
     * Menghapus seluruh entity pada tabel cluster
     */
    public static void deleteCluster() {
        String query = "DELETE FROM cluster";
        DatabaseHelper.execute(query);
    }
}
