/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;

import util.FileConfig;
import static database.DatabaseHelper.databaseConfig;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.experiment.InstanceQuery;

/**
 * Modul koneksi Weka dengan basis data.
 * @author Bagus Rahman Aryabima
 */
public class WekaDatabaseConnector {
    static InstanceQuery INSTANCE_QUERY;
    
    static FileConfig databaseConfig;
    
    /**
     * Membuat koneksi Weka dengan basis data.
     */
    public static void Connect() {
        try {
            databaseConfig = new FileConfig();
            databaseConfig.loadConfig("db/Database.conf");
            INSTANCE_QUERY = new InstanceQuery();
            INSTANCE_QUERY.setUsername(databaseConfig.getConfigValue().get("Username"));
            INSTANCE_QUERY.setPassword(databaseConfig.getConfigValue().get("Password"));
            INSTANCE_QUERY.setCustomPropsFile(new File("db/DatabaseUtils.props"));
        } catch (Exception ex) {
            Logger.getLogger(WekaDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Mengembalikan atribut static instanceQuery kelas ini.
     * @return Atribut static instanceQuery kelas ini.
     */
    public static InstanceQuery getInstanceQuery() {
        return INSTANCE_QUERY;
    }
}
