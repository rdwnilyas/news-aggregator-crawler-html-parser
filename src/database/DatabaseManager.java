/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import structure.News;

/**
 * Class to manage connection between Database and Application
 * 
 * @author Ahmad
 */
public class DatabaseManager {
    
    /***
     * Check if News is already exist
     * 
     * @param news
     * @return boolean true if exist
     */
    public static boolean isNewsExist(News news) {
        try {
            DatabaseHelper.Connect();
            String sql = "SELECT * FROM artikel WHERE URL=?";
            String parameters[] = new String[]{news.url};
            ResultSet res = DatabaseHelper.executeQuery(sql,parameters);
            if(res.next()) {
                DatabaseHelper.Disconnect();
                return true;
            } else {
                DatabaseHelper.Disconnect();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseHelper.Disconnect();
        }
        
        try {
            DatabaseHelper.Connect();
            String sql = "SELECT * FROM artikel WHERE URL like ? and JUDUL=?";
            String site = news.url.split("/")[2];
            String parameters[] = new String[]{"%"+site+"%", news.title};
            
            ResultSet res = DatabaseHelper.executeQuery(sql,parameters);
            if(res.next()) {
                DatabaseHelper.Disconnect();
                return true;
            } else {
                DatabaseHelper.Disconnect();
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseHelper.Disconnect();
            return false;
        }
    }
    
    /***
     * Save news
     * 
     * @param news
     * @return true if success 
     */
    public static boolean SaveNews(News news) {
        
        DatabaseHelper.Connect();
        String sql = "INSERT INTO artikel(FULL_TEXT,TGL_TERBIT,TGL_CRAWL,JUDUL,URL,INFO_WHAT,INFO_WHERE,INFO_WHO,INFO_WHEN,INFO_WHY,INFO_HOW) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        
        DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        DateFormat resFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date;
        String dateString = "";
        try {
            date = formatter.parse(news.getPubDate());
            dateString = resFormatter.format(date);
        } catch (ParseException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String nowString = resFormatter.format(new Date());
        
        String content = news.getContent();//.replaceAll("\\,", "");
        String title = news.getTitle();//.replaceAll("\\,", "");
        String parameter[] = {content, dateString, nowString, title, news.getUrl(), news.getWhat(),
                                news.getWhere(),news.getWho(),news.getWhen(),news.getWhy(),news.getHow()};
        
        int idx = DatabaseHelper.executeInsert(sql, parameter);
        DatabaseHelper.Disconnect();
        
        if(idx == -1) {
            return false;
        } else {
            // Save Image Link
            if(news.imageLink != null) {
                sql = "INSERT INTO gambar_artikel(ID_ARTIKEL,LOKASIFILE) VALUES (?,?)";
                DatabaseHelper.Connect();
                int relationIdx = DatabaseHelper.executeInsert(sql, new String[]{""+idx,""+news.imageLink});
                DatabaseHelper.Disconnect();
            }
            
            return true;
        }
    }
    
    /***
     * Save array of news
     * 
     * @param news_list
     * @return true if all the news are saved
     */
    public static boolean SaveNews(News[] news_list) {
        
        //DatabaseHelper.Connect();
        boolean res = true;
        for(int i=0; i < news_list.length; i++) {
            boolean tmp = SaveNews(news_list[i]);
            res = res && tmp;
        }
        //DatabaseHelper.Disconnect();
        return res;
    }
    
    /***
     * Get Category ID based on database
     * 
     * @param category_name category string 
     * @return id category based on database
     */
    public static int getKategori(String category_name) {
        DatabaseHelper.Connect();
        try {
            ResultSet res = DatabaseHelper.executeQuery("SELECT * FROM kategori WHERE LABEL like ?", new String[] {"%" + category_name + "%"});
            int idx = -1;
            
            if(res.next()) {
                idx =  res.getInt("ID_KELAS");
            } 
            
            DatabaseHelper.Disconnect();
            return idx;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            
            DatabaseHelper.Disconnect();
            return -1;
        }
    }
    
    /***
     * get list of news sources
     * @return array of news source in String[]
     */
    public static String[] getNewsSource() {
        try {
            DatabaseHelper.Connect();
            ResultSet results = DatabaseHelper.executeQuery("SELECT * FROM news_sources");
            ArrayList<String> resultList = new ArrayList<>();
            while(results.next()) {
                resultList.add(results.getString("url"));
            }
            DatabaseHelper.Disconnect();
            
            return resultList.toArray(new String[resultList.size()]);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseHelper.Disconnect();
            return null;
        }
    }
    
    public static Hashtable<String, Integer> GetConfig() {
        DatabaseHelper.Connect();
        String sql = "SELECT * FROM config";
        ResultSet res = DatabaseHelper.executeQuery(sql);
        try {
            Hashtable<String, Integer> results = new Hashtable<>();
            if(res.next()) {
                results.put("status", res.getInt("status"));
                results.put("durasi_parser", res.getInt("durasi_parser"));
                results.put("durasi_cluster", res.getInt("durasi_cluster"));
                DatabaseHelper.Disconnect();
                return results;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseHelper.Disconnect();
        }
        DatabaseHelper.Disconnect();
        return null;
    }
    
    public static void Log(String tag, String desc) {
        DatabaseHelper.Connect();
        String sql = "INSERT INTO log(tag,aktivitas) VALUES (?,?)";
        DatabaseHelper.executeInsert(sql, new String[] {tag,desc});
        DatabaseHelper.Disconnect();
    }
    
    public static ArrayList<News> GetUnprocessNews() {
        ArrayList<News> results = new ArrayList<>();
        DatabaseHelper.Connect();
        ResultSet resultSet = DatabaseHelper.executeQuery("SELECT * FROM artikel WHERE STATUS=0");
        try {
            while(resultSet.next()) {
                News news = new News();
                news.content = resultSet.getString("FULL_TEXT");
                news.title = resultSet.getString("JUDUL");
                news.url = resultSet.getString("URL");
                news.id = resultSet.getInt("ID_ARTIKEL");
                
                results.add(news);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        DatabaseHelper.Disconnect();
        return results;
    }
    
    public static boolean UpdateNews(News news) {
        
        boolean result;
        
        DatabaseHelper.Connect();
        
        String sql = "UPDATE artikel "
                + "SET FULL_TEXT=?, " 
                + "JUDUL=?, "
                + "URL=?,"
                + "INFO_WHAT=?,"
                + "INFO_WHERE=?,"
                + "INFO_WHO=?,"
                + "INFO_WHEN=?,"
                + "INFO_WHY=?,"
                + "INFO_HOW=?,"
                + "STATUS=1 "
                + "WHERE ID_ARTIKEL=?";
        
        result = DatabaseHelper.execute(sql, 
                new String[] {
                    news.content,
                    news.title,
                    news.url,
                    news.what,
                    news.where,
                    news.who,
                    news.when,
                    news.why,
                    news.how,
                    "" + news.id
                });
        
        DatabaseHelper.Disconnect();
        
        int category = getKategori(news.getCategory());
        if(category == -1) {
            System.out.println("Error Kategori " + news.getCategory());
        } else {
            DatabaseHelper.Connect();
            // Save Category
            sql = "INSERT INTO artikel_kategori VALUES (?,?) "
                    + "ON DUPLICATE KEY UPDATE "
                    + "ID_ARTIKEL=?, "
                    + "ID_KELAS=?";
            int relationIdx = DatabaseHelper.executeInsert(sql, new String[]{""+news.id,""+category,""+news.id,""+category});
            result = relationIdx >= 0 && result;
            DatabaseHelper.Disconnect();
        }
        
        return result;
    }
    
    public static boolean UpdateNews(ArrayList<News> newsList) {
        boolean result = true;
        
        for(News news : newsList) {
            result = UpdateNews(news) && result;
        }
        
        return result;
    }
    
}
