/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clustering;

import database.DatabaseHelper;
import database.WekaDatabaseConnector;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import structure.ArtikelKategori;
import structure.Cluster;
import structure.ClusterArtikel;
import weka.clusterers.Clusterer;
import weka.clusterers.FilteredClusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.DistanceFunction;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.tokenizers.AlphabeticTokenizer;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.unsupervised.attribute.NominalToString;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 *
 * @author MLK
 * @author Bagus Rahman Aryabima
 * Kelas ini direstructure dari kelas TextClustering yang ditulis Bagus Rahman Aryabima
*/

public class NewsClustering {
    
    private static ArrayList<Instances> baseTrain;
    private static ArrayList<Instances> tempTrain;
    //private static ArrayList<Clusterer> clusterer;
    private static int size;
    private static String[][] arrMedoidId;
    private static double[][] minDistance; 
    private static double[][] maxDistance; 
    private static double[][] avgDistance; 
    private static ArrayList<int[]> instCluster;
    private static Instances[] centroidSet;
    private static String[][] clusterName;
    private static int[][] count;
    
    /**
     * Melakukan temu balik data latih dan data uji yang digunakan modul pengklasteran teks.
     */
    public static void retrieveInstances() {
        DatabaseHelper.Connect();
        WekaDatabaseConnector.Connect();
        
        try {
            baseTrain = new ArrayList<>();
            tempTrain = new ArrayList<>();
            size = ArtikelKategori.countIDKelas();
            
           for (Integer I : ArtikelKategori.selectIDKelas()) {
                baseTrain.add(WekaDatabaseConnector.getInstanceQuery().retrieveInstances("SELECT artikel.ID_ARTIKEL, artikel.JUDUL, artikel.FULL_TEXT, artikel_kategori.ID_KELAS FROM artikel NATURAL JOIN artikel_kategori WHERE artikel_kategori.ID_KELAS = " + I));
//                
                tempTrain.add(baseTrain.get(baseTrain.size() - 1));
            }
        } catch (Exception ex) {
            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void buildClusterer(int numCluster) {
        try {
            FilteredClusterer clusterer;
            
            //MLK: abstraksi cluster: cari medoid, ambil info_what dari medoid
            arrMedoidId=new String[size][numCluster]; //id artikel setiap medoid cluster
            clusterName=new String[size][numCluster]; 
            minDistance=new double[size][numCluster]; 
            maxDistance=new double[size][numCluster]; 
            avgDistance=new double[size][numCluster]; 
            instCluster=new ArrayList<>();
            centroidSet=new Instances[size];
            count=new int[size][numCluster];
            
            for (int i = 0; i < size; i++) {
                clusterer = new FilteredClusterer();
                
                SimpleKMeans baseClusterer=new SimpleKMeans();
                String[] options = weka.core.Utils.splitOptions("-N "+numCluster);                
                baseClusterer.setOptions(options);
                baseClusterer.setPreserveInstancesOrder(true); //MLK
                
                clusterer.setClusterer(baseClusterer);
                                
                Filter[] arrFilter=new Filter[2];
                                
                arrFilter[0]=new NominalToString();
                arrFilter[0].setInputFormat(tempTrain.get(i));
                ((NominalToString) arrFilter[0]).setOptions(weka.core.Utils.splitOptions("-C 2,3"));
                        
                arrFilter[1]= new StringToWordVector();
                arrFilter[1].setInputFormat(tempTrain.get(i));
        //        strVector.setStopwords(new File("I:\\Java Project\\Learning\\src\\stopword.txt"));
                ((StringToWordVector) arrFilter[1]).setTFTransform(true);
                ((StringToWordVector) arrFilter[1]).setIDFTransform(true);
                ((StringToWordVector) arrFilter[1]).setMinTermFreq(7);
                ((StringToWordVector) arrFilter[1]).setLowerCaseTokens(true);        
                ((StringToWordVector) arrFilter[1]).setWordsToKeep(100);
                //weka.filters.unsupervised.attribute.StringToWordVector -R first-last -W 1000 -prune-rate -1.0 -N 1 -stemmer weka.core.stemmers.NullStemmer -M 1 -tokenizer "weka.core.tokenizers.WordTokenizer -delimiters \" \\r\\n\\t.,;:\\\'\\\"()?!\""
                WordTokenizer wordTokenizer = new WordTokenizer();
                String delimiters = " \r\t\n.,;:\'\"()?!-><#$\\%&*+/@^_=[]{}|`~0123456789 ”�–“•";
                wordTokenizer.setDelimiters(delimiters);

        //        AlphabeticTokenizer tokenizer = new AlphabeticTokenizer();        
        ////        NGramTokenizer tokenizer = new NGramTokenizer();        
        ////        tokenizer.setNGramMaxSize(3);
                ((StringToWordVector) arrFilter[1]).setTokenizer(wordTokenizer);
        
                MultiFilter mf=new MultiFilter();
                mf.setFilters(arrFilter);
                
                clusterer.setFilter(mf);

//weka.clusterers.FilteredClusterer -F "weka.filters.MultiFilter -F \"weka.filters.unsupervised.attribute.NominalToString -C 2,3\" -F \"weka.filters.unsupervised.attribute.StringToWordVector -R first-last -W 1000 -prune-rate -1.0 -N 0 -stemmer weka.core.stemmers.NullStemmer -M 1 -tokenizer \\\"weka.core.tokenizers.WordTokenizer -delimiters \\\\\\\" \\\\\\\\r\\\\\\\\n\\\\\\\\t.,;:\\\\\\\\\\\\\\\'\\\\\\\\\\\\\\\"()?!\\\\\\\"\\\"\"" -W weka.clusterers.SimpleKMeans -- -init 0 -max-candidates 100 -periodic-pruning 10000 -min-density 2.0 -t1 -1.25 -t2 -1.0 -N 5 -A "weka.core.EuclideanDistance -R first-last" -I 500 -num-slots 1 -S 10                
                
//                classifier.setFilter(filter1);
                
                

                clusterer.buildClusterer(tempTrain.get(i));
                
                //MLK: abstraksi cluster: cari medoid, ambil info_what dari medoid
                centroidSet[i]=((SimpleKMeans) clusterer.getClusterer()).getClusterCentroids();
                arrMedoidId[i]=new String[numCluster]; //id artikel setiap medoid cluster
                //ambil member dari setiap cluster
                int[] idxMember=new int[numCluster];
                
                double[] sum=new double[numCluster];
                for (int j=0;j<numCluster;j++) {
                    minDistance[i][j]=1e6;
                    maxDistance[i][j]=-1;
                    avgDistance[i][j]=-1;
                    idxMember[j]=-1;
                    count[i][j]=0;
                    sum[j]=0;
//                    Instance inst = centroidSet[i].get(j);
//                    double[] arrValues=inst.toDoubleArray();
                    
                }
                DistanceFunction distF=((SimpleKMeans) clusterer.getClusterer()).getDistanceFunction();
                int[] arrAssignment=((SimpleKMeans) clusterer.getClusterer()).getAssignments();
                instCluster.add(arrAssignment);
                for (int j=0;j<arrAssignment.length;j++) { //instCluster.length=tempTrain.size()
                    int idxCluster=arrAssignment[j];
                    double jarak=distF.distance(centroidSet[i].get(idxCluster),tempTrain.get(i).get(j));
                    if (minDistance[i][idxCluster]>jarak) {
                        minDistance[i][idxCluster]=jarak;
                        idxMember[idxCluster]=j;
                    }
                    if (maxDistance[i][idxCluster]<jarak) {
                        maxDistance[i][idxCluster]=jarak;
                    }
                    count[i][idxCluster]++;
                    sum[idxCluster]=sum[idxCluster]+jarak;
                }
                for (int j=0;j<numCluster;j++) {
                    if (idxMember[j]!=-1) {//member cluster==0
                        arrMedoidId[i][j]=baseTrain.get(i).instance(idxMember[j]).toString(0);
                        avgDistance[i][j]=sum[j]/count[i][j];
                    }
                }
                                                
            }
        } catch (Exception ex) {
            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    /**
     * Melakukan pengklasteran setiap instance anggota data uji.
     */
    public static void cluster(double thresholdAvgDist, int thresholdMemberSize) {
        try {
            String id_cluster;
            
            ClusterArtikel.deleteClusterArtikel();
            Cluster.deleteCluster();
            
            for (int i = 0; i < size; i++) {
                int[] arrAssignment=instCluster.get(i);
                for (int j = 0; j < tempTrain.get(i).numInstances(); j++) {
                    int clusterLabel = arrAssignment[j];
                    
                    id_cluster = ("0000000" + Integer.toString(clusterLabel + 1)).substring(Integer.toString(clusterLabel + 1).length());
                    id_cluster = Integer.toString(i + 2) + id_cluster;
                                        
                    //MLK: kenapa diassign sebanyak instance, harusnya 1 cluster n instance
                    if (arrMedoidId[i][clusterLabel]!=null)
                        Cluster.insertCluster(id_cluster, Integer.toString(i + 2),arrMedoidId[i][clusterLabel],minDistance[i][clusterLabel],maxDistance[i][clusterLabel],avgDistance[i][clusterLabel],((avgDistance[i][clusterLabel]<=thresholdAvgDist) && (count[i][clusterLabel]>=thresholdMemberSize)),centroidSet[i].get(clusterLabel).toString());
                    ClusterArtikel.insertClusterArtikel(baseTrain.get(i).instance(j).toString(0), id_cluster);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        retrieveInstances();
        System.out.println("Load instances ... completed");
        
        buildClusterer(5);
        System.out.println("Build clusters ... completed");
        
        cluster(10.0,2);
        System.out.println("Save clusters to db ... completed");
        
        DatabaseHelper.Connect();
        DatabaseHelper.execute("update cluster c set cluster_name=(select judul from artikel a where a.id_artikel=c.id_medoid)");
        DatabaseHelper.execute("update cluster c set ringkasan_standard=(select concat(info_who,' ',info_what,' di ',info_where,' pada ',info_when) from artikel a where a.id_artikel=c.id_medoid)");
        System.out.println("Update cluster name to db ... completed");
        DatabaseHelper.Disconnect();
    }
    
}


// kode Bagus sebelumnya untuk method cluster            
//            for (int i = 0; i < size; i++) {
//                for (int j = 0; j < tempTest.get(i).numInstances(); j++) {
//                    double clusterLabel = clusterer.get(i).clusterInstance(tempTest.get(i).instance(j));
//                    
//                    id_cluster = ("0000000" + Integer.toString((int) clusterLabel + 1)).substring(Integer.toString((int) clusterLabel + 1).length());
//                    id_cluster = Integer.toString(i + 2) + id_cluster;
//                    
//                    Cluster.insertCluster(id_cluster, Integer.toString(i + 2),arrMedoidId[(int)clusterLabel]);
//                    ClusterArtikel.insertClusterArtikel(baseTest.get(i).instance(j).toString(0), id_cluster);
//                }
//            }
