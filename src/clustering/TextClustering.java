// Nama         : Bagus Rahman Aryabima
// NIM          : 23513192
// Nama File    : TextClustering.java
// Tanggal      : 1 Oktober 2014

package clustering;

import database.DatabaseHelper;
import database.WekaDatabaseConnector;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import structure.ArtikelKategori;
import structure.Cluster;
import structure.ClusterArtikel;
import weka.clusterers.*;
import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Modul pengklasteran teks.
 * @author Bagus Rahman Aryabima
    //Kelas ini tidak digunakan lagi, edit di NewsClustering
 */
public class TextClustering {
    private static ArrayList<Instances> baseTrain;
    private static ArrayList<Instances> baseTest;
    private static ArrayList<Instances> tempTrain;
    private static ArrayList<Instances> tempTest;
    //private static ArrayList<Clusterer> clusterer;
    private static int size;
    private static String[][] arrMedoidId;
    private static double[][] minDistance; 
    private static double[][] maxDistance; 
    private static double[][] avgDistance; 
    private static ArrayList<int[]> instCluster;
    private static Instances[] centroidSet;
    private static String[][] clusterName;
    private static int[][] count;
    
    // =====================
    // ***** INSTANCES *****
    // =====================
    
    /**
     * Melakukan temu balik data latih dan data uji yang digunakan modul pengklasteran teks.
     */
    public static void retrieveInstances() {
        DatabaseHelper.Connect();
        WekaDatabaseConnector.Connect();
        
        try {
            baseTrain = new ArrayList<>();
            baseTest = new ArrayList<>();
            tempTrain = new ArrayList<>();
            tempTest = new ArrayList<>();
            //clusterer = new ArrayList<>();
            size = ArtikelKategori.countIDKelas();
            
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.MONTH, -1);
            
            for (Integer I : ArtikelKategori.selectIDKelas()) {
                // Baris berikut digunakan untuk menyertakan seluruh artikel bercluster sebagai basis clustering
//                baseTrain.add(WekaDatabaseConnector.getInstanceQuery().retrieveInstances("SELECT artikel.ID_ARTIKEL, artikel.JUDUL, artikel.FULL_TEXT, artikel_kategori.ID_KELAS FROM artikel NATURAL JOIN artikel_kategori LEFT JOIN cluster_artikel ON artikel.ID_ARTIKEL = cluster_artikel.ID_ARTIKEL WHERE cluster_artikel.ID_CLUSTER IS NOT NULL AND artikel_kategori.ID_KELAS = " + (i + 1)));                
                // Baris berikut digunakan untuk menyertakan artikel bercluster berusia maksimal sebulan sebagai basis clustering
//                baseTrain.add(WekaDatabaseConnector.getInstanceQuery().retrieveInstances("SELECT artikel.ID_ARTIKEL, artikel.JUDUL, artikel.FULL_TEXT, artikel_kategori.ID_KELAS FROM artikel NATURAL JOIN artikel_kategori LEFT JOIN cluster_artikel ON artikel.ID_ARTIKEL = cluster_artikel.ID_ARTIKEL WHERE cluster_artikel.ID_CLUSTER IS NOT NULL AND artikel_kategori.ID_KELAS = " + (i + 1) + " AND artikel_kategori.ID_KELAS = 14 AND artikel.TGL_CRAWL > '" + simpleDateFormat.format(calendar.getTime()) + "'"));
                
                baseTrain.add(WekaDatabaseConnector.getInstanceQuery().retrieveInstances("SELECT artikel.ID_ARTIKEL, artikel.JUDUL, artikel.FULL_TEXT, artikel_kategori.ID_KELAS FROM artikel NATURAL JOIN artikel_kategori WHERE artikel_kategori.ID_KELAS = " + I));
                
//                baseTest.add(WekaDatabaseConnector.getInstanceQuery().retrieveInstances("SELECT artikel.ID_ARTIKEL, artikel.JUDUL, artikel.FULL_TEXT, artikel_kategori.ID_KELAS FROM artikel NATURAL JOIN artikel_kategori LEFT JOIN cluster_artikel ON artikel.ID_ARTIKEL = cluster_artikel.ID_ARTIKEL WHERE cluster_artikel.ID_CLUSTER IS NULL AND artikel_kategori.ID_KELAS = " + (i + 1)));
                baseTest.add(WekaDatabaseConnector.getInstanceQuery().retrieveInstances("SELECT artikel.ID_ARTIKEL, artikel.JUDUL, artikel.FULL_TEXT, artikel_kategori.ID_KELAS FROM artikel NATURAL JOIN artikel_kategori WHERE artikel_kategori.ID_KELAS = " + I));
                
                tempTrain.add(baseTrain.get(baseTrain.size() - 1));
                tempTest.add(baseTest.get(baseTrain.size() - 1));
            }
        } catch (Exception ex) {
            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Menerapkan filter pada data latih dan data uji yang digunakan modul pengklasteran teks.
     */
    public static void useFilter() {
        ArrayList<Instances> temp = new ArrayList<>();
        ArrayList<Integer> attributeIndex = new ArrayList<>();
        
        attributeIndex.add(1);
        attributeIndex.add(2);
        
        for (int i = 0; i < size; i++) {
            tempTrain.set(i, FilterManager.Remove(tempTrain.get(i), 1));
            tempTest.set(i, FilterManager.Remove(tempTest.get(i), 1));
            
            tempTrain.set(i, FilterManager.NominalToString(tempTrain.get(i), attributeIndex));
            tempTest.set(i, FilterManager.NominalToString(tempTest.get(i), attributeIndex));
            
//            tempTrain.set(i, FilterManager.StringToWordVector(tempTrain.get(i), attributeIndex));
            temp = FilterManager.BatchStringToWordVector(tempTrain.get(i), tempTest.get(i), attributeIndex);
            tempTrain.set(i, temp.get(0));
            tempTest.set(i, temp.get(1));
        }
    }
    
    // =====================
    // ***** CLUSTERER *****
    // =====================
    
    /**
     * Membangun Clusterer yang digunakan modul pengklasteran teks.
     */
    public static void buildClusterer(int numCluster) {
        try {
//            DBSCAN dbscan;
            SimpleKMeans clusterer;
            
            //MLK: abstraksi cluster: cari medoid, ambil info_what dari medoid
            arrMedoidId=new String[size][numCluster]; //id artikel setiap medoid cluster
            clusterName=new String[size][numCluster]; 
            minDistance=new double[size][numCluster]; 
            maxDistance=new double[size][numCluster]; 
            avgDistance=new double[size][numCluster]; 
            instCluster=new ArrayList<>();
            centroidSet=new Instances[size];
            count=new int[size][numCluster];
            
            for (int i = 0; i < size; i++) {
//                dbscan = new DBSCAN();
                clusterer = new SimpleKMeans();
                
                // Ubah baris berikut untuk mengatur parameter clusterer.
                // -E adalah radius setiap cluster.
                // -M adalah jumlah anggota minimal setiap cluster.
//                String[] options = weka.core.Utils.splitOptions("-E 7.5 -M 1");
                String[] options = weka.core.Utils.splitOptions("-N "+numCluster);
                
//                dbscan.setOptions(options);
//                dbscan.buildClusterer(tempTrain.get(i));
//                clusterer.add(dbscan);
                clusterer.setOptions(options);
                clusterer.setPreserveInstancesOrder(true); //MLK
                clusterer.buildClusterer(tempTrain.get(i));

                //MLK: abstraksi cluster: cari medoid, ambil info_what dari medoid
                centroidSet[i]=clusterer.getClusterCentroids();
                arrMedoidId[i]=new String[numCluster]; //id artikel setiap medoid cluster
                //ambil member dari setiap cluster
                int[] idxMember=new int[numCluster];
                
                double[] sum=new double[numCluster];
                for (int j=0;j<numCluster;j++) {
                    minDistance[i][j]=1e6;
                    maxDistance[i][j]=-1;
                    avgDistance[i][j]=-1;
                    idxMember[j]=-1;
                    count[i][j]=0;
                    sum[j]=0;
//                    Instance inst = centroidSet[i].get(j);
//                    double[] arrValues=inst.toDoubleArray();
                    
                }
                DistanceFunction distF=clusterer.getDistanceFunction();
                int[] arrAssignment=clusterer.getAssignments();
                instCluster.add(arrAssignment);
                for (int j=0;j<arrAssignment.length;j++) { //instCluster.length=tempTrain.size()
                    int idxCluster=arrAssignment[j];
                    double jarak=distF.distance(centroidSet[i].get(idxCluster),tempTrain.get(i).get(j));
                    if (minDistance[i][idxCluster]>jarak) {
                        minDistance[i][idxCluster]=jarak;
                        idxMember[idxCluster]=j;
                    }
                    if (maxDistance[i][idxCluster]<jarak) {
                        maxDistance[i][idxCluster]=jarak;
                    }
                    count[i][idxCluster]++;
                    sum[idxCluster]=sum[idxCluster]+jarak;
                }
                for (int j=0;j<numCluster;j++) {
                    if (idxMember[j]!=-1) {//member cluster==0
                        arrMedoidId[i][j]=baseTrain.get(i).instance(idxMember[j]).toString(0);
                        avgDistance[i][j]=sum[j]/count[i][j];
                    }
                }
                                                
            }
        } catch (Exception ex) {
            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    /**
     * Memuat suatu model yang berada di file bernama fileName untuk digunakan model pengklasteran teks.
     * @param index Indeks pada Array List Clusterer yang bersesuaian dengan ID kategori.
     * @param fileName Nama file model yang digunakan di modul pengklasteran teks.
     */
//    public static void loadModel(int index, String fileName) {
//        try {
//            clusterer.set(index, (Clusterer) weka.core.SerializationHelper.read(fileName));
//        } catch (Exception ex) {
//            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    /**
     * Memuat seluruh model yang berada di file bernama fileName untuk digunakan model pengklasteran teks.
     * @param fileName Nama umum kumpulan file model yang digunakan di modul pengklasteran teks.
     */
//    public static void loadModel(String fileName) {
//        try {
//            for (int i = 0; i < size; i++) {
//                clusterer.set(i, (Clusterer) weka.core.SerializationHelper.read(fileName + i + ".model"));
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    /**
     * Menyimpan suatu model yang digunakan modul pengklasteran teks ke file bernama fileName.
     * @param index Indeks pada Array List Clusterer yang bersesuaian dengan ID kategori.
     * @param fileName Nama file model yang digunakan di modul pengklasteran teks.
     */
//    public static void saveModel(int index, String fileName) {
//        try {
//            weka.core.SerializationHelper.write(fileName, clusterer.get(index));
//        } catch (Exception ex) {
//            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    
    /**
     * Menyimpan suatu model yang digunakan modul pengklasteran teks ke file bernama fileName.
     * @param fileName Nama umum kumpulan file model yang digunakan di modul pengklasteran teks.
     */
//    public static void saveModel(String fileName) {
//        try {
//            for (int i = 0; i < size; i++) {
//                weka.core.SerializationHelper.write(fileName + i + ".model", clusterer.get(i));
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    /**
     * Melakukan pengklasteran setiap instance anggota data uji pada suatu kategori.
     * @param index Indeks pada Array List Clusterer yang bersesuaian dengan ID kategori.
     */
//    public static void cluster(int index) {
//        try {
//            String id_cluster;
//            
//            for (int i = 0; i < tempTest.get(index).numInstances(); i++) {
//                double clusterLabel = clusterer.get(index).clusterInstance(tempTest.get(index).instance(i));
//                
//                id_cluster = ("0000000" + Integer.toString((int) clusterLabel + 1)).substring(Integer.toString((int) clusterLabel + 1).length());
//                id_cluster = Integer.toString(index + 2) + id_cluster;
//                
////                Cluster.insertCluster(id_cluster, Integer.toString(index));
////                ClusterArtikel.insertClusterArtikel(baseTest.get(index).instance(i).toString(0), id_cluster);
//                
//                System.out.println(id_cluster);
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    
    /**
     * Melakukan pengklasteran setiap instance anggota data uji.
     */
    public static void cluster(double thresholdAvgDist, int thresholdMemberSize) {
        try {
            String id_cluster;
            
            ClusterArtikel.deleteClusterArtikel();
            Cluster.deleteCluster();
            
            for (int i = 0; i < size; i++) {
                int[] arrAssignment=instCluster.get(i);
                for (int j = 0; j < tempTrain.get(i).numInstances(); j++) {
                    int clusterLabel = arrAssignment[j];
                    
                    id_cluster = ("0000000" + Integer.toString(clusterLabel + 1)).substring(Integer.toString(clusterLabel + 1).length());
                    id_cluster = Integer.toString(i + 2) + id_cluster;
                                        
                    //MLK: kenapa diassign sebanyak instance, harusnya 1 cluster n instance
                    if (arrMedoidId[i][clusterLabel]!=null)
                        Cluster.insertCluster(id_cluster, Integer.toString(i + 2),arrMedoidId[i][clusterLabel],minDistance[i][clusterLabel],maxDistance[i][clusterLabel],avgDistance[i][clusterLabel],((avgDistance[i][clusterLabel]<=thresholdAvgDist) && (count[i][clusterLabel]>=thresholdMemberSize)),centroidSet[i].get(clusterLabel).toString());
                    ClusterArtikel.insertClusterArtikel(baseTrain.get(i).instance(j).toString(0), id_cluster);
                }
            }
            
//            for (int i = 0; i < size; i++) {
//                for (int j = 0; j < tempTest.get(i).numInstances(); j++) {
//                    double clusterLabel = clusterer.get(i).clusterInstance(tempTest.get(i).instance(j));
//                    
//                    id_cluster = ("0000000" + Integer.toString((int) clusterLabel + 1)).substring(Integer.toString((int) clusterLabel + 1).length());
//                    id_cluster = Integer.toString(i + 2) + id_cluster;
//                    
//                    Cluster.insertCluster(id_cluster, Integer.toString(i + 2),arrMedoidId[(int)clusterLabel]);
//                    ClusterArtikel.insertClusterArtikel(baseTest.get(i).instance(j).toString(0), id_cluster);
//                }
//            }
        } catch (Exception ex) {
            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // ======================
    // ***** EVALUATION *****
    // ======================
    
    /**
     * Mengevaluasi hasil pengklasteran modul pengklasteran teks pada suatu kategori.
     * @param index Indeks pada Array List Clusterer yang bersesuaian dengan ID kategori.
     */
//    public static void evaluate(int index) {
//        try {
//            ClusterEvaluation clusterEvaluation = new ClusterEvaluation();
//            clusterEvaluation.setClusterer(clusterer.get(index));
//            clusterEvaluation.evaluateClusterer(tempTest.get(index));
//            
//            System.out.println("# of clusters: " + clusterEvaluation.getNumClusters());
//            
//        } catch (Exception ex) {
//            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    /**
     * Mengevaluasi hasil pengklasteran modul pengklasteran teks.
     */
//    public static void evaluate() {
//        try {
//            for (int i = 0; i < size; i++) {
//                ClusterEvaluation clusterEvaluation = new ClusterEvaluation();
//                clusterEvaluation.setClusterer(clusterer.get(i));
//                clusterEvaluation.evaluateClusterer(tempTest.get(i));
//
//                System.out.println("# of clusters: " + clusterEvaluation.getNumClusters());
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(TextClustering.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    //Kelas ini tidak digunakan lagi, edit di NewsClustering
//    public static void main(String[] args) {
//        retrieveInstances();
//        useFilter();
//        buildClusterer(5);
//        cluster(5.0,3);
//        DatabaseHelper.Connect();
//        DatabaseHelper.execute("update cluster c set cluster_name=(select info_what from artikel a where a.id_artikel=c.id_medoid)");
//    }
}
