/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package informationextraction;

import java.util.Scanner;

/**
 *
 * @author MLK
 * kelas untuk menyimpan informasi slot filler
 */
public class Filler {
    public String text;
    public String tag;
    String tokenBegin;
    String tokenEnd;
    
    public Filler(String txt,String vtag) {
        text=txt;
        tag=vtag;
        if (!text.equals("-")) {
            //prepared to be compared with string in labeling token
            
            String[] arr=text.split(" ");
            if (text.startsWith("(")) tokenBegin="(";
            else tokenBegin=arr[0];
            if ((tokenBegin.endsWith(",")) || (tokenBegin.endsWith("."))) tokenBegin=tokenBegin.replaceFirst("[,.]","");
            if (text.endsWith(")")) tokenEnd=")";                
            else tokenEnd=arr[arr.length-1];
            if ((tokenEnd.endsWith(",")) || (tokenEnd.endsWith("."))) tokenEnd=tokenEnd.replaceFirst("[,.]","");
        }
        else {
            tokenBegin="";
            tokenEnd="";
        }
    }
    
    public void updateToken(String prevtag, String token) {
        //i.s: ((stc.Label.get(k).equals("beg_"<tag>))||(stc.Label.get(k).equals("in_"<tag>)))
        token=token.replaceAll("'", "");
        if ((prevtag.equals("beg_"+tag))||(prevtag.equals("in_"+tag))) {
            text=text+" "+token;
        }
        else {
            if (text.startsWith(token)) 
                text=token;
            else 
                text=text+"; "+token;
        }
        tokenEnd=token;
    }
                
    public void print(String header) {
        System.out.println(header+text+" - "+tokenBegin+" - "+tokenEnd);
    }
}
