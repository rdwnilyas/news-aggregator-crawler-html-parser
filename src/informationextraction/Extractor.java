/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package informationextraction;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author MLK
 * Kelas untuk mengekstraksi 5W1H dari "satu" artikel yang belum berlabel
 */
public class Extractor {
    
    Classifier cls;
    Instances trainingData;
    
    public Extractor (String modelFile){
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(modelFile)));
            cls = (Classifier) objectInputStream.readObject();
            //System.out.println(cls.toString());
            trainingData = (Instances) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception ex) {
            Logger.getLogger(Extractor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                objectInputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Extractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
//        try {
//            cls= (Classifier) weka.core.SerializationHelper.read(modelFile);        
//        } catch (Exception ex) {
//            Logger.getLogger(Extractor.class.getName()).log(Level.SEVERE, null, ex);
//        }                
    }
    
    public static Instances loadData (String filename) throws Exception { 
    //i.s: file "filename" berformat weka dan valid. ClassIndex yang terakhir   
    //return data1 berisi semua data dari filename
        Instances result = ConverterUtils.DataSource.read(filename);
        if (result.classIndex() == -1)
            result.setClassIndex(result.numAttributes() - 1);
        return result;    
    }
    
    ArrayList<Filler> filling(NewsArticle article) {
        HashMap<String,Filler> result=new HashMap<>();
        //proses filling
        String prev_label="other";
        for (int j=0;j<article.sentences.size();j++) {
            Sentence stc=article.sentences.get(j);
            for (int k=0;k<stc.Label.size();k++) {
                //beg_who,in_who,beg_what,in_what,beg_when,in_when,beg_where,in_where,beg_why,in_why,beg_how,in_how,other
                if (!stc.Label.get(k).equals("other")) {
                    if ((stc.Label.get(k).equals("beg_who"))||(stc.Label.get(k).equals("in_who"))) {
                        Filler f=result.get("who"); 
                        if (f==null){
                            f=new Filler(stc.token.get(k).replaceAll("'", ""),"who");
//                            f.tokenBegin=stc.token.get(k);
//                            f.tokenEnd=stc.token.get(k);
                        }        
                        else {
                            f.updateToken(prev_label, stc.token.get(k));
                        }
                        result.put("who", f);
                    } 
                    else if ((stc.Label.get(k).equals("beg_what"))||(stc.Label.get(k).equals("in_what"))) {
                        Filler f=result.get("what"); 
                        if (f==null){
                            f=new Filler(stc.token.get(k).replaceAll("'", ""),"what");
//                            f.tokenBegin=stc.token.get(k);
//                            f.tokenEnd=stc.token.get(k);
                        }        
                        else {
                            f.updateToken(prev_label, stc.token.get(k));
                        }
                        result.put("what", f);
                    }
                    else if ((stc.Label.get(k).equals("beg_when"))||(stc.Label.get(k).equals("in_when"))) {
                        Filler f=result.get("when"); 
                        if (f==null){
                            f=new Filler(stc.token.get(k).replaceAll("'", ""),"when");
//                            f.tokenBegin=stc.token.get(k);
//                            f.tokenEnd=stc.token.get(k);
                        }        
                        else {
                            f.updateToken(prev_label, stc.token.get(k));
                        }
                        result.put("when", f);
                    }
                    else if ((stc.Label.get(k).equals("beg_where"))||(stc.Label.get(k).equals("in_where"))) {
                        Filler f=result.get("where"); 
                        if (f==null){
                            f=new Filler(stc.token.get(k).replaceAll("'", ""),"where");
//                            f.tokenBegin=stc.token.get(k);
//                            f.tokenEnd=stc.token.get(k);
                        }        
                        else {
                            f.updateToken(prev_label, stc.token.get(k));
                        }
                        result.put("where", f);
                    }
                    else if ((stc.Label.get(k).equals("beg_why"))||(stc.Label.get(k).equals("in_why"))) {
                        Filler f=result.get("why"); 
                        if (f==null){
                            f=new Filler(stc.token.get(k).replaceAll("'", ""),"why");
//                            f.tokenBegin=stc.token.get(k);
//                            f.tokenEnd=stc.token.get(k);
                        }        
                        else {
                            f.updateToken(prev_label, stc.token.get(k));
                        }
                        result.put("why", f);
                    }
                    else if ((stc.Label.get(k).equals("beg_how"))||(stc.Label.get(k).equals("in_how"))) {
                        Filler f=result.get("how"); 
                        if (f==null){
                            f=new Filler(stc.token.get(k).replaceAll("'", ""),"how");
//                            f.tokenBegin=stc.token.get(k);
//                            f.tokenEnd=stc.token.get(k);
                        }        
                        else {
                            f.updateToken(prev_label, stc.token.get(k));
                        }
                        result.put("how", f);
                    }//else if
                }//if !other
                prev_label=stc.Label.get(k);
            }
        }
        
        ArrayList<Filler> arr=new ArrayList<>();
        Iterator it = result.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry item = (Map.Entry) it.next();
            //System.out.println(item.getKey() + " = " + ((Filler)item.getValue()).text.replaceAll("'",""));
            arr.add((Filler)item.getValue());
        }        
        return arr;
    }
    
    public ArrayList<Filler> process (String judul, String text) {
        //i.s: sembarang
        //return 0:What,1:Who,2:Where,3:When,4:Why,5:How
        NewsArticle article=new NewsArticle(judul,text, false);
        article.process(); //proses ekstraksi fitur setiap token
        classifyToken(article); //setiap token sudah diprediksi labelnya
            
        return filling(article);
    }
    
    private void classifyToken(NewsArticle article) {
            //i.s: NEList, token, tokenKind,cf,mf,posf setiap kalimat sudah terdefinisi
            //f.s: stc.label berisi hasil prediksi tag setiap token
            Instances data = new Instances(trainingData,0,1);
            data.setClassIndex(0);
            //System.out.println("Klasifikasi token: ");
            //Instances result=new Instances(trainingData);
            String bef1="<start>";
            String bef2="<start>";
            String labelBef1="start";
            String labelBef2="start";
            String curr;
            for (int j=0;j<article.sentences.size();j++) {
                Sentence stc=article.sentences.get(j);
                stc.Label=new ArrayList<>();
                //System.out.println("\n=========================\n"+stc.text);
                for (int k=0;k<stc.token.size();k++) {
                    try {
                        Instance inst = data.firstInstance();
                        inst.setValue(trainingData.attribute(14), labelBef2);
                        inst.setValue(trainingData.attribute(15), bef2);
                        if (bef2.equals("<start>")) {
                            inst.setValue(trainingData.attribute(16), "_NULL_");
                            inst.setValue(trainingData.attribute(17), "_NULL_");
                            inst.setValue(trainingData.attribute(18), "_NULL_");
                            inst.setValue(trainingData.attribute(19), "_NULL_");
                            inst.setValue(trainingData.attribute(20), "OTHER");
                        }
                        else {
                            inst.setValue(trainingData.attribute(16), inst.value(trainingData.attribute(9)));
                            inst.setValue(trainingData.attribute(17), inst.value(trainingData.attribute(10)));
                            inst.setValue(trainingData.attribute(18), inst.value(trainingData.attribute(11)));
                            inst.setValue(trainingData.attribute(19), inst.value(trainingData.attribute(12)));
                            inst.setValue(trainingData.attribute(20), inst.value(trainingData.attribute(13)));
                        }
                        inst.setValue(trainingData.attribute(7), labelBef1);
                        inst.setValue(trainingData.attribute(8), bef1);
                        if (bef1.equals("<start>")) {
                            inst.setValue(trainingData.attribute(9), "_NULL_");
                            inst.setValue(trainingData.attribute(10), "_NULL_");
                            inst.setValue(trainingData.attribute(11), "_NULL_");
                            inst.setValue(trainingData.attribute(12), "_NULL_");
                            inst.setValue(trainingData.attribute(13), "OTHER");
                        }
                        else {                            
                            inst.setValue(trainingData.attribute(9), inst.value(trainingData.attribute(2)));
                            inst.setValue(trainingData.attribute(10), inst.value(trainingData.attribute(3)));
                            inst.setValue(trainingData.attribute(11), inst.value(trainingData.attribute(4)));
                            inst.setValue(trainingData.attribute(12), inst.value(trainingData.attribute(5)));
                            inst.setValue(trainingData.attribute(13), inst.value(trainingData.attribute(6)));
                        }                            
                        curr=stc.token.get(k).replaceAll("'", "");
                        inst.setValue(trainingData.attribute(1), curr);
                        inst.setValue(trainingData.attribute(2), stc.tokenKind.get(k));
                        if (trainingData.attribute(3).indexOfValue(stc.posf.get(k))==-1) {
                            inst.setValue(trainingData.attribute(3), "_NULL_");
                        }
                        else inst.setValue(trainingData.attribute(3), stc.posf.get(k));
                        inst.setValue(trainingData.attribute(4), stc.cf.get(k));
                        inst.setValue(trainingData.attribute(5), stc.mf.get(k));
                        inst.setValue(trainingData.attribute(6), stc.NEList.get(k));
                        
                        inst.setValue(trainingData.attribute(21), String.valueOf(j==0));
                        inst.setValue(trainingData.attribute(22), String.valueOf(stc.time.get(k)));
                        inst.setValue(trainingData.attribute(23), String.valueOf(k==stc.token.size()-1));
                        int idx = article.arrTokenJudul.indexOf(curr.toLowerCase());
                        inst.setValue(trainingData.attribute(23), String.valueOf(idx!=-1));
    
                        System.out.println("\nData: "+inst.toString());            
                        double pred = cls.classifyInstance(inst);   
                        System.out.println("Hasil prediksi "+pred+" - "+inst.classAttribute().value((int)pred));            
                                            
                        stc.Label.add(inst.classAttribute().value((int)pred));
                                
                        bef2=bef1;
                        labelBef2=labelBef1;
                        bef1=curr;
                        labelBef1=inst.classAttribute().value((int)pred);
                    } catch (Exception ex) {
                        Logger.getLogger(Extractor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }        
    }

    public ArrayList<ArrayList<Filler>> processBatch (NewsCorpus nc) {
        //i.s: nc terdefinisi, dan nc.proses sudah dilakukan
        //return untuk setiap article: 0:What,1:Who,2:Where,3:When,4:Why,5:How
        
        ArrayList<ArrayList<Filler>> result=new ArrayList<>();
        for (int i=0;i<nc.corpus.size();i++) {
            NewsArticle article=nc.corpus.get(i);
            //article.proses sudah dilakukan pada nc.proses
            classifyToken(article); //setiap token sudah diprediksi labelnya
            ArrayList<Filler> arr=filling(article);
            result.add(arr);
        }
                    
        return result;
    }
    
    public static void main(String[] args) throws Exception
    {
        Extractor extractor=new Extractor("I:\\Riset\\2014\\news_aggregator\\5W1H\\j48_30_191114.mod");
        
        //contoh ekstraksi 5w1h dari korpus artikel
        //NewsCorpus nc=new NewsCorpus("I:\\Riset\\2014\\news_aggregator\\5W1H\\log\\kelompok.log");
        NewsCorpus nc=new NewsCorpus(false);
        nc.process();                
        ArrayList<ArrayList<Filler>> arr=extractor.processBatch(nc);
        for (int j=0;j<arr.size();j++) {
            NewsArticle article=nc.corpus.get(j);
            System.out.println(article.judul);
            System.out.println(article.text);
            System.out.println("\nHasil ekstraksi 5W1H: ");
            for (int i=0;i<arr.get(j).size();i++) {
                if (arr.get(j).get(i).tag.equalsIgnoreCase("what"))
                    article.strWhat.text=arr.get(j).get(i).text.replaceAll(" comma ", " , ");
                else if (arr.get(j).get(i).tag.equalsIgnoreCase("who"))
                    article.strWho.text=arr.get(j).get(i).text.replaceAll(" comma ", " , ");
                else if (arr.get(j).get(i).tag.equalsIgnoreCase("when"))
                    article.strWhen.text=arr.get(j).get(i).text.replaceAll(" comma ", " , ");
                else if (arr.get(j).get(i).tag.equalsIgnoreCase("where"))
                    article.strWhere.text=arr.get(j).get(i).text.replaceAll(" comma ", " , ");
                else if (arr.get(j).get(i).tag.equalsIgnoreCase("why"))
                    article.strWhy.text=arr.get(j).get(i).text.replaceAll(" comma ", " , ");
                else if (arr.get(j).get(i).tag.equalsIgnoreCase("how"))
                    article.strHow.text=arr.get(j).get(i).text.replaceAll(" comma ", " , ");
                System.out.println(arr.get(j).get(i).tag+" : "+arr.get(j).get(i).text);
            }
            article.executeUpdate();
        }
        //contoh ekstraksi 5w1h dari 1 artikel
//        ArrayList<Filler> arr=extractor.process("Canda Menhub Jonan: Saya Cuma Jaga Stasiun Kereta Saja Bisa Jadi Menteri",
//                "Jakarta -Menteri Perhubungan (Menhub) Ignasius Jonan menekankan pentingnya pelayanan saat blusukan ke kantor Badan Meteorologi Klimatologi dan Geofisika (BMKG), Kamayoran, Jakarta, sore ini. "
//                + "Selain pelayanan, para abdi negara termasuk para pegawai BMKG juga harus pandai 'jualan', bisa mengetahui apa kebutuhan masyarakat, dan bisa bermanfaat untuk orang banyak. "
//                + "Ia menceritakan soal keberhasilannya mengubah PT KAI menjadi perusahaan yang fokus pada pelayanan dan pelanggan. "
//                + "Sambil bercanda, Jonan mengatakan keberhasilannya kini membuahkan hasil, dipercaya oleh Presiden Jokowi sebagai menteri. "
//                + "\"Saya jaga kereta jadi Menhub. "
//                + "Ini fakta. "
//                + "Saya itu jaga stasiun saja jadi menteri. "
//                + "Bayangkan. "
//                + "Kalau bapak tanya di luar kepala BMKG itu siapa, nggak ada yang tahu? Tapi tanya siapa dirut KAI itu. "
//                + "Ibu-ibu di airport itu pulang umroh minta foto semua,\" kata Jonan berseloroh di depan para pegawai BMKG, Jakarta, Rabu (19/11/2014). "
//                + "Jonan menambahkan, sebagai abdi negara juga tak boleh membuang-buang sumber daya yang ada, seperti di Kementerian Perhubungan ada Pusat Penelitian dan Pengembangan (Puslitbang). "
//                + "Puslitbang Kemenhub harus dimaksimalkan agar bisa berguna untuk masyarakat. "
//                + "\"Litbang itu harus kampanye bahwa gunanya itu ada, itu penting sekali,\" katanya. "
//                + "Ia juga menekankan pentingnya marketing bagi aparat pemerintah. "
//                + "Jonan mengilustrasikan banyak grup-grup besar musik dunia dari berbagai negara justru terkenal karena 'dikemas' di Amerika Serikat (AS) . "
//                + "\"Saya tak menyarankan bapak sampai situ. BMKG kalau dipasarkan makin besar,\" katanya. "
//                + "Jonan mengungkapkan BMKG harus meningkatkan perannya di masyarakat, bisa menjawab tantangan kebutuhan, dan dikenal masyarakat. "
//                + "Misalnya soal pemasangan early warning system cuaca di UPT (Unit Pelaksana Teknis), syahbandar, dan Otban (Otoritas Bandara) agar digunakan untuk membaca perubahan cuaca demi demi keselamatan transportasi. "
//                + "\"Menhub baru tahu ada kantor BMKG di sini, hari ini. "
//                + "Bapak bisa kira-kira sendiri (kenapa bisa kayak gitu). "
//                + "Teman-teman Kemenhub itu belajarnya keras,\" katanya. "
//                + "(hen/dnl) Berita ini juga dapat dibaca melalui m.detik.com dan aplikasi detikcom untuk BlackBerry, Android, iOS & Windows Phone. "
//                + "Install sekarang!");//Jangkau Daerah Terpencil, BRI Bangun Bank Terapung","TEMPO.CO, Jakarta - PT Bank Rakyat Indonesia (BRI) memperluas layanan perbankan melalui bank terapung bernama Teras BRI Kapal. Direktur Jaringan dan Layanan BRI Suprajarto mengatakan Teras BRI Kapal bisa melayani masyarakat pesisir dan daerah-daerah terpencil. (Baca: Mengembangkan UMKM dengan Teras BRI) \"Selama ini daerah tersebut menjadi blind spot untuk layanan perbankan,\" kata dia di galangan PT Dumas Tanjung Perak Shipyard, Surabaya, Senin, 10 November 2014. Kehadiran Teras BRI Kapal, kata Suprajarto, bisa membantu meningkatkan kesejahteraan masyarakat. Bagi BRI, bank terapung ini bisa menggenjot dana pihak ketiga (DPK) serta meningkatkan edukasi perbankan dan inklusi keuangan pada masyarakat. (Baca: Kuartal III, BRI Raup Laba Bersih Rp 18 Triliun) Suprajarto mengatakan untuk membangun satu buah Teras BRI Kapal dibutuhkan dana Rp 13 miliar. Bank terapung ini rencana akan mulai beroperasi secara berkeliling pada 2015. Saat ini pembangunan kapal baru memasuki tahap keel laying atau peletakan lunas. BRI meminta PT Dumas agar merampungkan kapal ini pada Desember 2014. \"Sehingga bisa beroperasi pada Februari 2015.\" Direktur Utama Dumas Tanjung Perak Shipyard Yance Gunawan mengatakan BRI baru memesan satu buah kapal. Idealnya, kata Yance, pembangunan kapal ini memakan waktu delapan bulan. AISHA SHAIDRA Berita ");
//        System.out.println("Hasil ekstraksi 5W1H: ");
//        for (int i=0;i<arr.size();i++) {
//            System.out.println(arr.get(i).tag+" : "+arr.get(i).text);
//        }
    }
    
}
