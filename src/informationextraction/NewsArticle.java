/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package informationextraction;


import IndonesianNLP.IndonesianNETagger;
import IndonesianNLP.IndonesianSentenceDetector;
import IndonesianNLP.IndonesianSentenceTokenizer;
import database.DatabaseHelper;
import database.DatabaseManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author MLK
 * kelas artikel berita yang berisi teks dan informasi 5W1H
 */
public class NewsArticle {
    String oriText,
           text; //clean text
    String judul;
    Filler strWhat,
           strWho,
           strWhere,
           strWhen,
           strWhy,
           strHow;
    int id;
    ArrayList<Sentence> sentences;   
    ArrayList<String> arrTokenJudul;
    boolean training;
    
    public NewsArticle(String vjudul, String konten, boolean vtraining) {
        //training=true: konten merupakan article dgn 5W1H tags
        initialize();
        judul=vjudul;
        oriText=konten;        
        sentences=new ArrayList<>();
        training=vtraining;
        if (training) {
            text=cleanTags(konten);
            read5W1HTag();
        }
        else text=konten;
    }
    
    public void process() {
        //f.s: label 5W1H sudah berhasil dibaca; semua fitur setiap token didapatkan
        IndonesianSentenceTokenizer tokenizer=new IndonesianSentenceTokenizer();
        arrTokenJudul=tokenizer.tokenizeSentence(judul.toLowerCase());
        System.out.println("Judul: "+arrTokenJudul.toString());//+" - "+arrTokenJudul.indexOf("tender"));
        
        IndonesianSentenceDetector detector=new IndonesianSentenceDetector();
        ArrayList<String> arr=detector.splitSentence(text);        
        
        //pisahkan header dgn kalimat pertama
        //System.out.println(arr.get(0));
        String[] first=arr.get(0).split("( - | -- |: | -)");
//        System.out.println(first[0]);
//        System.out.println(first[1]);
        
        if (first.length>1) {
            arr.add(0,first[0]);
            arr.set(1,first[1]);
        }
        
        for (int i=0; i<arr.size();i++) {            
            //System.out.println(arr.get(i));
            sentences.add(extractNEFeatures(arr.get(i)));
        }
    }
    
    public void print() {
        //i.s: sentences terdefinisi
        //f.s: semua informasi per token ditampilkan ke layar
//        System.out.println("Original text: "+oriText);
        System.out.println("Text without 5W1H tags: "+text);
        System.out.println("Label 5W1H tags: "+text);
        strWho.print("Who: ");
        strWhat.print("What: ");
        strWhen.print("When: ");
        strWhere.print("Where: ");
        strWhy.print("Why: ");
        strHow.print("How: ");
        System.out.println();
        for (int i=0; i<sentences.size();i++) {
            sentences.get(i).print();
            System.out.println();
        }
        System.out.println();
    }
    
    private Sentence extractNEFeatures (String stc) {
        Sentence sentence=new Sentence(stc);
        sentence.print();
        
        if (training) {
            int idxBeginWho=-1;
            int idxEndWho=-1;            
            int idWho=stc.indexOf(strWho.text);
            if (idWho!=-1) {
                idxBeginWho=sentence.token.indexOf("'"+strWho.tokenBegin+"'");
                idxEndWho=sentence.token.indexOf("'"+strWho.tokenEnd+"'");
            }

            int idxBeginWhat=-1;
            int idxEndWhat=-1;            
            int idWhat=stc.indexOf(strWhat.text);
            if (idWhat!=-1) {
                idxBeginWhat=sentence.token.indexOf("'"+strWhat.tokenBegin+"'");
                idxEndWhat=sentence.token.indexOf("'"+strWhat.tokenEnd+"'");
            }

            int idxBeginWhen=-1;
            int idxEndWhen=-1;            
            int idWhen=stc.indexOf(strWhen.text);
            if (idWhen!=-1) {
                idxBeginWhen=sentence.token.indexOf("'"+strWhen.tokenBegin+"'");
                idxEndWhen=sentence.token.indexOf("'"+strWhen.tokenEnd+"'");            
            }

            int idxBeginWhere=-1;
            int idxEndWhere=-1;            
            int idWhere=stc.indexOf(strWhere.text);
            if (idWhere!=-1) {
                idxBeginWhere=sentence.token.indexOf("'"+strWhere.tokenBegin+"'");
                idxEndWhere=sentence.token.indexOf("'"+strWhere.tokenEnd+"'");
            }

            int idxBeginWhy=-1;
            int idxEndWhy=-1;            
            int idWhy=stc.indexOf(strWhy.text);
            if (idWhy!=-1) {
                idxBeginWhy=sentence.token.indexOf("'"+strWhy.tokenBegin+"'");
                idxEndWhy=sentence.token.indexOf("'"+strWhy.tokenEnd+"'");            
            }

            int idxBeginHow=-1;
            int idxEndHow=-1;            
            int idHow=stc.indexOf(strHow.text);
            if (idHow!=-1) {
                idxBeginHow=sentence.token.indexOf("'"+strHow.tokenBegin+"'");
                idxEndHow=sentence.token.indexOf("'"+strHow.tokenEnd+"'");            
            }

            sentence.Label=new ArrayList<>();
            for (int i=0;i<sentence.token.size();i++) {
                if (i==idxBeginWho) 
                    sentence.Label.add("beg_who");
                else if ((i>idxBeginWho) && (i<=idxEndWho))
                    sentence.Label.add("in_who");
                else if (i==idxBeginWhat) 
                    sentence.Label.add("beg_what");
                else if ((i>idxBeginWhat) && (i<=idxEndWhat))
                    sentence.Label.add("in_what");
                else if (i==idxBeginWhen) 
                    sentence.Label.add("beg_when");
                else if ((i>idxBeginWhen) && (i<=idxEndWhen))
                    sentence.Label.add("in_when");
                else if (i==idxBeginWhere) 
                    sentence.Label.add("beg_where");
                else if ((i>idxBeginWhere) && (i<=idxEndWhere))
                    sentence.Label.add("in_where");
                else if (i==idxBeginWhy) 
                    sentence.Label.add("beg_why");
                else if ((i>idxBeginWhy) && (i<=idxEndWhy))
                    sentence.Label.add("in_why");
                else if (i==idxBeginHow) 
                    sentence.Label.add("beg_how");
                else if ((i>idxBeginHow) && (i<=idxEndHow))
                    sentence.Label.add("in_how");
                else 
                    sentence.Label.add("other");
            }

        }

        return sentence;
    }
        
    private String cleanTags(String text) {
        text=text.replaceFirst("_bwho_","");
        text=text.replaceFirst("_ewho_","");
        text=text.replaceFirst("_bwhat_","");
        text=text.replaceFirst("_ewhat_","");
        text=text.replaceFirst("_bwhere_","");
        text=text.replaceFirst("_ewhere_","");
        text=text.replaceFirst("_bwhen_","");
        text=text.replaceFirst("_ewhen_","");
        text=text.replaceFirst("_bwhy_","");
        text=text.replaceFirst("_ewhy_","");
        text=text.replaceFirst("_bhow_","");
        text=text.replaceFirst("_ehow_","");        
        return text;
    }
    
    private void initialize() {
        strWhat=new Filler("-","what");
        strWho=new Filler("-","who");
        strWhere=new Filler("-","where");
        strWhen=new Filler("-","when");
        strWhy=new Filler("-","why");
        strHow=new Filler("-","how");
        sentences=new ArrayList<>();
        arrTokenJudul=new ArrayList<>();
    }
    
    public void executeUpdate() {
        //i.s: strWhat, strWho, dst sudah terdefinisi
        //f.s: info 5w1h update ke db
        
        DatabaseHelper.Connect();
        String sql = "UPDATE artikel set INFO_WHAT=?, INFO_WHERE=?, INFO_WHO=?,INFO_WHEN=?,INFO_WHY=?,INFO_HOW=? where id_artikel=?";
      
        String parameter[] = {strWhat.text,strWhere.text,strWho.text,strWhen.text,strWhy.text,strHow.text, String.valueOf(id)};
        
        int idx = DatabaseHelper.executeUpdate(sql, parameter);
        DatabaseHelper.Disconnect();
        
        if(idx == -1) {
            System.out.println("Update info 5w1h artikel id "+id+" gagal");
        } else 
            System.out.println("Update info 5w1h artikel id "+id+" berhasil");
            // Save Category
    }
    
    private void read5W1HTag() {
        int idBeginWho=oriText.indexOf("_bwho_");
        if (idBeginWho!=-1) {
            int idEndWho=oriText.indexOf("_ewho_");
            strWho=new Filler(oriText.substring(idBeginWho+6, idEndWho),"who");
            //System.out.println("Who: "+oriText.substring(idBeginWho+6, idEndWho));
        }
        int idBeginWhat=oriText.indexOf("_bwhat_");
        if (idBeginWhat!=-1) {
            int idEndWhat=oriText.indexOf("_ewhat_");
            strWhat=new Filler(cleanTags(oriText.substring(idBeginWhat+7, idEndWhat)),"what");
            //System.out.println("What: "+oriText.substring(idBeginWhat+7, idEndWhat));                        
        }
        int idBeginWhen=oriText.indexOf("_bwhen_");
        if (idBeginWhen!=-1) {
            int idEndWhen=oriText.indexOf("_ewhen_");
            strWhen=new Filler(oriText.substring(idBeginWhen+7, idEndWhen),"when");
            //System.out.println("When: "+oriText.substring(idBeginWhen+7, idEndWhen));
        }
        int idBeginWhere=oriText.indexOf("_bwhere_");
        if (idBeginWhere!=-1) {
            int idEndWhere=oriText.indexOf("_ewhere_");
            strWhere=new Filler(oriText.substring(idBeginWhere+8, idEndWhere),"where");
            //System.out.println("Where: "+oriText.substring(idBeginWhere+8, idEndWhere));
        }
        int idBeginWhy=oriText.indexOf("_bwhy_");
        if (idBeginWhy!=-1) {
            int idEndWhy=oriText.indexOf("_ewhy_");
            strWhy=new Filler(oriText.substring(idBeginWhy+6, idEndWhy),"why");
            //System.out.println("Why: "+oriText.substring(idBeginWhy+6, idEndWhy));
        }
        int idBeginHow=oriText.indexOf("_bhow_");
        if (idBeginHow!=-1) {
            int idEndHow=oriText.indexOf("_ehow_");                    
            strHow=new Filler(oriText.substring(idBeginHow+6, idEndHow),"how");
            //System.out.println("How: "+oriText.substring(idBeginHow+6, idEndHow));
        }

    }
    
       public static void main(String[] args) {
           //NewsArticle article=new NewsArticle("XYZ","Sentani, Papua (ANTARA News) - _bwhat_Soal Ujian Nasional (UN) mulai didistribusikan ke distrik-distrik terjauh di _bwhere_Kabupaten Jayapura, Papua_ewhere__ewhat_, kata Kepala _bwho_Dinas Pendidikan Kabupaten Jayapura_ewho_, Alpius Toam, ST.MMT kepada Antara di Sentani, Minggu. \"Pendistribusian soal UN ini mulai dilaksanakan pada hari _bwhen_Jumat (11/4)_ewhen_ ke distrik yang paling jauh di wilayah pembangunan empat,\" ujarnya. Alpius mengatakan _bwhy_Ujian Nasional akan dilaksanakan serentak pada 15-18 April 2013_ewhy_, namun ada beberapa wilayah yang sulit dijangkau, yaitu Kaureh, Yapsi, Demta dan Airu. Menurut Alpius, distribusi soal UN akan dikawal ketat aparat Polres Jayapura guna menghindari hal-hal tidak diinginkan.  Polisi juga akan dikerahkan untuk ikut menjaga sewaktu Ujian Nasional berlangsung. \"Soal UN ini juga merupakan soal yang merupakan rahasia Negara, jangan sampai terjadi kebocoran,\" imbuhnya. Alpius mengungkapkan, jumlah peserta Ujian Nasional di Kabupaten Jayapura ada 7.389 peserta yang terdiri dari 1.403 tingkat SMA/MA, SMK sebanyak 484 peserta, SMP/Mts 2.319 peserta, sedangkan SD 2.577 peserta.", true);
           //article.process();
           
           
//            IndonesianSentenceDetector isd=new IndonesianSentenceDetector();
//            System.out.println(isd.akronim.toString());
//            ArrayList<String> arr=isd.splitSentence("Ini Budi. Ini Ibu Budi.");                   
//            for (int i=0;i<arr.size();i++) {
//                System.out.println(arr.get(i));
//            }
       }
    
}

