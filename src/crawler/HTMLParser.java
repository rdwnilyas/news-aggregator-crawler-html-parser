/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler;

/**
 *
 * @author Ahmad
 */
public interface HTMLParser {
    
    public boolean parse(String url);
    public String getTextContent();
    public String getImage();
    
}
