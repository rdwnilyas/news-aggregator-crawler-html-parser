/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package html_parser;

import crawler.HTMLParser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author Ahmad
 */
public class VivanewsParser implements HTMLParser{
    
    String content = "";
    String imageLink = "";
    
    @Override
    public boolean parse(String url) {
        content = "";
        try {
            Document doc = Jsoup.connect(url)
                    .timeout(50000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get();
            
            // Extract Content
            Element artikel = doc.select(".isiberita").first();
            content += artikel.text();
            
            // Extract Image URL
            Element foto = doc.select(".content-images-details").first();
            if(foto != null) {
                Element img = foto.select("img").first();
                //System.out.println(img);
                if(img != null)
                    imageLink = img.attr("src");
            }
            return true;
        } catch (IOException ex) {
            Logger.getLogger(TempoParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(TempoParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false; 
    }

    @Override
    public String getTextContent() {
        return content;
    }

    @Override
    public String getImage() {
        return imageLink;
    }
    
}
