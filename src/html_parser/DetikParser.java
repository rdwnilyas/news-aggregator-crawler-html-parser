/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package html_parser;

import crawler.HTMLParser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author Ahmad
 */
public class DetikParser implements HTMLParser {
    
    String content = "";
    String imageLink = "";
    
    @Override
    public boolean parse(String url) {
        content = "";
        try {
            Document doc = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get();
            
            //System.out.println(url);
            //System.out.println(doc.html());
            
            // Extract Content
            Element artikel = doc.select(".text_detail").first();
            if(artikel == null) return false; 
            content += artikel.text();
            
            //System.out.println(content);
            
            // Extract Image URL
            Element foto = doc.select(".pic_artikel").first();
            if(foto == null)  return true;
            //System.out.println(doc.select(".pic_artikel"));
            Element img = foto.select("img").first();
            //System.out.println(img);
            imageLink = img.attr("src");
            
            return true;
        } catch (IOException ex) {
            Logger.getLogger(TempoParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String getTextContent() {
        return content;
    }

    @Override
    public String getImage() {
        return imageLink;
    }
    
}
