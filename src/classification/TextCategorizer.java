/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classification;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.AbstractClassifier;
import weka.core.Instance;
import weka.core.Instances;

/**
 * @author MLK
 * Kelas untuk memprediksi kategori dari data teks
 */

public class TextCategorizer {
    
    AbstractClassifier cls;
    Instances trainingData;
    
    public TextCategorizer (String modelFile){
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(modelFile)));
            cls = (AbstractClassifier) objectInputStream.readObject();
            trainingData = (Instances) objectInputStream.readObject();
            objectInputStream.close();
            System.out.println("Load model -- completed ");//+fc.toString());
        } catch (ClassNotFoundException|IOException ex) {
            Logger.getLogger(TextCategorizer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                objectInputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(TextCategorizer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    public String predict(String[] atr, String[] teks) {
        try{           
            Instances data = new Instances(trainingData,0,1);
            data.setClassIndex(2);
            Instance inst_co = data.firstInstance();   
            for (int i=0;i<atr.length;i++) {
                inst_co.setValue(data.attribute(atr[i]),teks[i]);
            }
            //label pada data input diabaikan saja
            System.out.println("Teks input (abaikan nilai kelas awal): "+inst_co.toString());
                                   
            double pred = cls.classifyInstance(data.instance(0));   
            System.out.println("Hasil prediksi "+pred+" - "+data.classAttribute().value((int)pred));            
            return data.classAttribute().value((int)pred);
        }
        catch(Exception ex){
            Logger.getLogger(TextCategorizer.class.getName()).log(Level.SEVERE, null, ex);      
        }
        return null;
    }
    
}
